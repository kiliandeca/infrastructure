resource "google_container_cluster" "minekloud-euw1-c" {
  name               = "minekloud-euw1-c"
  location           = var.location

  remove_default_node_pool = true
  initial_node_count       = 1

}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "minekloud-node-pool-1"
  location   = var.location
  cluster    = google_container_cluster.minekloud-euw1-c.name
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = "e2-standard-2"
  }
}

resource "google_container_node_pool" "primary_nodes" {
  name       = "minekloud-node-pool-2"
  location   = var.location
  cluster    = google_container_cluster.minekloud-euw1-c.name
  node_count = 3

  node_config {
    machine_type = "e2-standard-2"
  }
}
