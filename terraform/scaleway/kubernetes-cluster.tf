terraform {
  backend "http" {
  }
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      version = "~> 1.17"
    }
  }
}

provider "scaleway" {
  # ENV VAR
  # access_key      = ""
  # secret_key      = ""
  # organization_id = ""
  zone            = "fr-par-1"
  region          = "fr-par"
}

resource "scaleway_k8s_cluster_beta" "k8s-minekloud" {
  name = "k8s-minekloud"
  version = "1.19"
  cni = "cilium"

  auto_upgrade {
      enable = true
      maintenance_window_start_hour = 8
      maintenance_window_day = "any"
  }
}

variable "pool_size" {
  type = number
}

resource "scaleway_k8s_pool_beta" "main-pool" {
  count = var.pool_size > 0 ? 1 : 0 # Hack to disable this block if pool size = 0

  cluster_id = scaleway_k8s_cluster_beta.k8s-minekloud.id
  name = "main-pool"
  node_type = "DEV1-M"
  size = var.pool_size
  autohealing = true
  container_runtime = "docker"
  wait_for_pool_ready = false
}